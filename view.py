import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser2


JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for


class View(webapp2.RequestHandler):
	def get(self,key_id):
		self.response.headers['Content-Type'] = 'text/html'
		search_key= ndb.Key('MyUser2',key_id)
		#get current user
		temp_user = users.get_current_user()
		temp_user_key = ndb.Key('MyUser2',temp_user.user_id())
		current_user = temp_user_key.get()


		search_results = search_key.get()
		search_results_final = []
		tweet_list = []

		button_value = "Follow"

		if key_id in current_user.following_list:
			button_value = "Unfollow"

		for row in search_results.tweets:
			temp={
			'content':row.content,
			'timestamp':row.timestamp
			}
			tweet_list.append(temp)

		sorted_tweet_list = sorted(tweet_list, key = lambda x : x['timestamp'], reverse=True)
		sorted_tweet_list = tweet_list[0:49]

		template_data={
			"search_key":search_results,
			"tweets":sorted_tweet_list,
			"button_value": button_value,
			"following":current_user.following_list


		}

		template = JINJA_ENVIRONMENT.get_template('view.html')
		self.response.write(template.render(template_data))


	def post(self,key_id):

		switch = self.request.get("follow_button")

		user_to_follow_key = ndb.Key("MyUser2",key_id)
		user_to_follow = user_to_follow_key.get()

		temp_user = users.get_current_user()
		temp_user_key = ndb.Key('MyUser2',temp_user.user_id())
		current_user = temp_user_key.get()

		if switch == "Follow":
			current_user.following_list.append(key_id)
			user_to_follow.followers_list.append(temp_user.user_id())
			current_user.put()
			user_to_follow.put()

		elif switch == "Unfollow":
			current_user.following_list.remove(key_id)
			user_to_follow.followers_list.remove(temp_user.user_id())
			current_user.put()
			user_to_follow.put()

		self.redirect('/')
