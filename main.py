import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser2
from edit import Edit
from tweets import Tweet
from search import Search
from view import View




JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		# URL that will contain a login or logout link
		url = ''
		url_string = ''
		welcome_message = 'Welcome back'
		# pull the current user from the request
		user = users.get_current_user()

		template_debug = ''
		display_string=''
		myuser=''
		all_tweets = []
		tweet_list=[]

		if user:
			url= users.create_logout_url(self.request.uri)

			url_string = 'logout'

			myuser_key = ndb.Key('MyUser2',user.user_id())
			myuser = myuser_key.get()


			switch = self.request.get('button')
			if myuser == None:
				welcome_message = 'Welcome to the application'

				myuser = MyUser2(id = user.user_id())
				myuser_key = myuser.put()

			elif switch == 'Add Username':
				temp_username = self.request.get('username')
				temp_user = myuser_key.get()
				temp_user.username = temp_username
				temp_user.put()
				self.redirect('/')

			elif switch == 'Tweet!':
				temp_tweet = self.request.get('add_tweet')
				temp_user = myuser_key.get()
				template_debug = temp_user
				new_tweet = Tweet(content=temp_tweet)
				temp_user.tweets.append(new_tweet)
				temp_user.put()
				self.redirect('/')

			else:
				all_tweets.append(myuser)
				for user_id in myuser.following_list:
					temp_user_key = ndb.Key('MyUser2', user_id)
					temp_user = temp_user_key.get()
					all_tweets.append(temp_user)


		else:
			url = users.create_login_url(self.request.uri)
			url_string = 'Login'

		template_values ={
			'url':url,
			'url_string': url_string,
			'display_string': display_string,
			'user': myuser,
			'welcome_message':welcome_message,
			'template_debug': template_debug,
			'tweets': all_tweets
		}

		template = JINJA_ENVIRONMENT.get_template('main.html')
		self.response.write(template.render(template_values))


app = webapp2.WSGIApplication([
webapp2.Route(r'/', handler=MainPage, name = 'mainpage'),
webapp2.Route(r'/edit/<key_id:\w+>', handler=Edit, name='edit'),
webapp2.Route(r'/edit', handler=Edit),
webapp2.Route(r'/search', handler=Search, name = 'search'),
webapp2.Route(r'/view', handler=View),
webapp2.Route(r'/view/<key_id:\w+>', handler=View, name='view'),
],debug=True)
