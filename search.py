import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser2

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for


class Search(webapp2.RequestHandler):
	def post(self):
		self.redirect('/')


	def get(self):
		self.response.headers['Content-Type'] = 'text/html'

		search_results = []
		search_results_final = []
		display_string=''
		debug=''

		temp_user = users.get_current_user()
		temp_user_key = ndb.Key('MyUser2',temp_user.user_id())
		current_user = temp_user_key.get()

		# user_search_list = MyUser2.query()
		switch = self.request.get('button')
		if switch=='Search User':
			search_username = self.request.get('search_user')
			search_results_final = MyUser2.query(MyUser2.username == search_username)
			search_results_final = search_results_final.filter(MyUser2.username != current_user.username).fetch()

			display_string = 'User Search'



		elif switch=='Search Tweets':
			search_tweets = self.request.get('search_tweets')
			user = users.get_current_user()
			search_results = MyUser2.query().fetch()
			search_results_final = []
			for row in search_results:
				for content in row.tweets:
					if search_tweets in content.content:

						temp = {
						'username':row.username,
						"tweets":content
						}

						search_results_final.append(temp)
			display_string = 'Tweet Search'

		template_search_values = {
		'results' : search_results_final,
		'display_string': display_string,
		'debug' : debug
		}

		template = JINJA_ENVIRONMENT.get_template('search.html')
		self.response.write(template.render(template_search_values))
