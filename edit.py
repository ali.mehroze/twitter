import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser2


JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class Edit(webapp2.RequestHandler):
	def get(self,key_id):
		self.response.headers['Content-Type'] = 'text/html'
		template_data = {}
		#get current user
		search_user_key= ndb.Key('MyUser2',key_id)
		switch = self.request.get('button')

		if switch == 'Edit':

			template_data={
				"search_key":search_user_key.get(),
				"display_string":switch
			}

			template = JINJA_ENVIRONMENT.get_template('edit.html')
			self.response.write(template.render(template_data))
		elif switch == 'Edit Tweet':
			tweet_content = self.request.get('tweet_content')

			template_data={
				"old_tweet":tweet_content,
				"display_string" : switch,
				"search_key":search_user_key.get()
			}

			template = JINJA_ENVIRONMENT.get_template('edit.html')
			self.response.write(template.render(template_data))

		elif switch== 'Delete':
			temp_user = search_user_key.get()
			temp_tweet = self.request.get('tweet_content')

			temp_user.tweets = [index for index in temp_user.tweets if index.content!=temp_tweet]
			temp_user.put()

			self.redirect('/')

	def post(self):
		self.response.headers['Content-Type'] = 'text/html'
		#code here
		if self.request.get('button')=='Update':
			name = self.request.get('name')
			description = self.request.get('description')
			user_id = self.request.get('id')

			new_user_key = ndb.Key('MyUser2',user_id)
			new_user = new_user_key.get()
			new_user.name = name
			new_user.description = description
			new_user.put()

		#Update tweet content
		elif self.request.get('button')=='Update Tweet':
			user_id = self.request.get('id')
			old_tweet = self.request.get('old_tweet')
			new_tweet = self.request.get('new_tweet')

			search_user_key = ndb.Key('MyUser2',user_id)
			search_user = search_user_key.get()

			for index in search_user.tweets:
				if(index.content == old_tweet ):
					index.content = new_tweet

			search_user.put()

			self.redirect('/')

		elif self.request.get('button') == 'Cancel':
			self.redirect('/')
